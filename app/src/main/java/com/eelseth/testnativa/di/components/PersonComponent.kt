package com.eelseth.testnativa.di.components

import com.eelseth.testnativa.di.AppComponent
import com.eelseth.testnativa.di.modules.PersonModule
import com.eelseth.testnativa.di.scopes.MainScope
import dagger.Component

/**
 * Created by eelSeth on 1/20/2018.
 */

//@MainScope
//@Component(dependencies = [AppComponent::class], modules = [PersonModule::class])
//interface PersonComponent : AppComponent {
//
//    @Component.Builder
//    interface Builder {
//
//        fun appComponent(baseComponent: AppComponent): Builder
//
//        fun build(): PersonComponent
//
//    }
//}