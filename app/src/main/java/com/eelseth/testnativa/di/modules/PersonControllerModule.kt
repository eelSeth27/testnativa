package com.eelseth.testnativa.di.modules

import com.eelseth.testnativa.persistence.AppDatabase
import com.eelseth.testnativa.person.controllers.PersonController
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 * Created by eelSeth on 1/20/2018.
 */

@Module
class PersonControllerModule {

    @Provides
    @Singleton
    fun provideFirebaseDBService(): DatabaseReference {
        return FirebaseDatabase.getInstance().reference.child("persons")
    }

    @Provides
    @Singleton
    fun personController(appDatabase: AppDatabase, cloudDatabase: DatabaseReference): PersonController {
        return PersonController(appDatabase, cloudDatabase)
    }

}