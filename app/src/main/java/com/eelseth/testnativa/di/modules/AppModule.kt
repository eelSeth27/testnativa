package com.eelseth.testnativa.di.modules

import android.app.Application
import android.content.Context
import com.eelseth.testnativa.organization.controllers.OrganizationController
import com.eelseth.testnativa.persistence.AppDatabase
import com.eelseth.testnativa.person.controllers.PersonController
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 * Created by eelSeth on 1/20/2018.
 */

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun provideDBService(context: Context): AppDatabase {
        return AppDatabase.getInstance(context)
    }

    @Provides
    @Singleton
    fun organizationController(appDatabase: AppDatabase): OrganizationController {
        return OrganizationController(appDatabase)
    }

}