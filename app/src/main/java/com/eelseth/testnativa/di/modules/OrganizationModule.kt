package com.eelseth.testnativa.di.modules

import androidx.lifecycle.ViewModel
import com.eelseth.testnativa.base.di.ViewModelKey
import com.eelseth.testnativa.organization.OrganizationViewModel
import com.eelseth.testnativa.organization.add.AddOrganizationFragment
import com.eelseth.testnativa.organization.add.AddOrganizationViewModel
import com.eelseth.testnativa.organization.OrganizationFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap


/**
 * Created by eelSeth on 1/20/2018.
 */

@Module
abstract class OrganizationModule {

    @ContributesAndroidInjector
    abstract fun bindOrganizationFragment(): OrganizationFragment

    @ContributesAndroidInjector
    abstract fun bindAddOrganizationFragment(): AddOrganizationFragment

    @Binds
    @IntoMap
    @ViewModelKey(OrganizationViewModel::class)
    abstract fun bindOrganizationViewModel(organizationViewModel: OrganizationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddOrganizationViewModel::class)
    abstract fun bindAddOrganizationViewModel(addOrganizationViewModel: AddOrganizationViewModel): ViewModel

}