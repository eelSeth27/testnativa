package com.eelseth.testnativa.di.modules

import androidx.lifecycle.ViewModel
import com.eelseth.testnativa.base.di.ViewModelKey
import com.eelseth.testnativa.person.PersonFragment
import com.eelseth.testnativa.person.PersonViewModel
import com.eelseth.testnativa.person.add.AddPersonFragment
import com.eelseth.testnativa.person.add.AddPersonViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap


/**
 * Created by eelSeth on 1/20/2018.
 */

@Module
abstract class PersonModule {

    @ContributesAndroidInjector
    abstract fun bindPersonFragment(): PersonFragment

    @ContributesAndroidInjector
    abstract fun bindAddPersonFragment(): AddPersonFragment

    @Binds
    @IntoMap
    @ViewModelKey(PersonViewModel::class)
    abstract fun bindPersonViewModel(personViewModel: PersonViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddPersonViewModel::class)
    abstract fun bindAddPersonViewModel(addPersonViewModel: AddPersonViewModel): ViewModel


}