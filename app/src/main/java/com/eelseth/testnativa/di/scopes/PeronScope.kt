package com.eelseth.testnativa.di.scopes

import javax.inject.Scope

/**
 * Created by eelSeth on 1/20/2018.
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PeronScope