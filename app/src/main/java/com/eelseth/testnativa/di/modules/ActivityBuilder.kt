package com.eelseth.testnativa.di.modules

import com.eelseth.testnativa.HomeActivity
import com.eelseth.testnativa.di.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by eelSeth on 1/20/2018.
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindHomeActivity(): HomeActivity

}