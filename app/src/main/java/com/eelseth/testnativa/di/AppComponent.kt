package com.eelseth.testnativa.di

import android.app.Application
import com.eelseth.testnativa.ApplicationHandler
import com.eelseth.testnativa.base.di.DaggerViewModelInjectionModule
import com.eelseth.testnativa.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by eelSeth on 1/20/2018.
 */

@Singleton
@Component(
        modules = [
            AppModule::class,
            PersonModule::class,
            PersonControllerModule::class,
            OrganizationModule::class,
            ActivityBuilder::class,
            AndroidSupportInjectionModule::class,
            DaggerViewModelInjectionModule::class
        ])

interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

    fun inject(app: ApplicationHandler)

}