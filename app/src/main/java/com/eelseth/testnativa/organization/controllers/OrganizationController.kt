package com.eelseth.testnativa.organization.controllers

import com.eelseth.testnativa.model.Organization
import com.eelseth.testnativa.persistence.AppDatabase
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by eelSeth on 2/27/2018.
 */

@Singleton
class OrganizationController @Inject constructor(private val appDatabase: AppDatabase) {

    fun getAllOrganizations(): Flowable<List<Organization>> = appDatabase.organizationDao().getAllOrganizations()

    fun saveOrganization(organization: Organization): Observable<Unit> =
            Observable.fromCallable { appDatabase.organizationDao().insertOrganization(organization) }

}