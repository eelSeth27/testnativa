package com.eelseth.testnativa.organization.add

import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.eelseth.testnativa.R
import com.eelseth.testnativa.base.ui.fragment.BaseDialogFragment
import com.eelseth.testnativa.databinding.FormOrganizationBinding
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by eelSeth on 1/13/2018.
 */

class AddOrganizationFragment : BaseDialogFragment() {

    private val binding by lazy { FormOrganizationBinding.inflate(LayoutInflater.from(context)) }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    internal lateinit var viewModel: AddOrganizationViewModel

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        setStyle(androidx.fragment.app.DialogFragment.STYLE_NO_FRAME, R.style.AppTheme)
        return super.onCreateDialog(savedInstanceState).apply {
            setCanceledOnTouchOutside(true)
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.attributes?.windowAnimations = R.style.DialogAnimation
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AddOrganizationViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        setUpToolbar(binding.toolbar, R.string.screen_add_organization)
        binding.viewModel = viewModel
    }

    override fun initSubscriptions() {
        super.initSubscriptions()
        compositeDisposable.add(
                viewModel.actionsSubjectObservable().subscribe(this::handleActions)
        )
    }

    private fun handleActions(action: AddOrganizationUiModel) {
        when (action) {
            AddOrganizationUiModel.OnSuccessfullySaved -> showSuccessfulAlert()
        }
    }

    private fun showSuccessfulAlert() {
        Snackbar.make(binding.root, R.string.message_successfully_added, Snackbar.LENGTH_LONG).show()
    }

    companion object {
        fun newInstance() = AddOrganizationFragment()
    }
}