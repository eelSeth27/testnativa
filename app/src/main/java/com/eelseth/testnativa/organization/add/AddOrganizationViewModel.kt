package com.eelseth.testnativa.organization.add

import androidx.lifecycle.ViewModel
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.util.Log
import com.eelseth.testnativa.base.NO_ERROR
import com.eelseth.testnativa.base.extensions.applyIoMain
import com.eelseth.testnativa.base.extensions.className
import com.eelseth.testnativa.model.Organization
import com.eelseth.testnativa.organization.controllers.OrganizationController
import com.eelseth.testnativa.util.clearFormFields
import com.eelseth.testnativa.util.getData
import com.eelseth.testnativa.util.isFieldFilled
import com.eelseth.testnativa.util.isFormValid
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.*
import javax.inject.Inject

/**
 * Created by eelSeth on 1/13/2018.
 */

class AddOrganizationViewModel @Inject constructor(private val organizationController: OrganizationController) : ViewModel() {

    private val actionsSubject by lazy { PublishSubject.create<AddOrganizationUiModel>() }

    val name = ObservableField("")
    val phone = ObservableField("")
    val address = ObservableField("")

    val errorName = ObservableInt(NO_ERROR)
    val errorPhone = ObservableInt(NO_ERROR)
    val errorAddress = ObservableInt(NO_ERROR)

    val loadingVisibility = ObservableBoolean(false)

    fun validateForm() {
        if (isFormValid(errorName.isFieldFilled(name.getData()),
                        errorPhone.isFieldFilled(phone.getData()),
                        errorAddress.isFieldFilled(address.getData()))) {
            save()
        }
    }

    private fun save() {
        organizationController.saveOrganization(Organization(0, name.getData(), phone.getData(), address.getData()))
                .doOnSubscribe { loadingVisibility.set(true) }
                .delay(1L, TimeUnit.SECONDS)
                .applyIoMain()
                .subscribe({
                    Log.e(this.className(), "Inserted 1 item in DB...")
                    loadingVisibility.set(false)
                    actionsSubject.onNext(AddOrganizationUiModel.OnSuccessfullySaved)
                    clearForm()
                }, {
                    it.printStackTrace()
                    loadingVisibility.set(false)
                })
    }

    private fun clearForm() {
        clearFormFields(name, phone, address)
    }

    fun actionsSubjectObservable(): Observable<AddOrganizationUiModel> = actionsSubject
}

sealed class AddOrganizationUiModel {
    object OnSuccessfullySaved : AddOrganizationUiModel()
}