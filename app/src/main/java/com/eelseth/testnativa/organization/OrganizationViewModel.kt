package com.eelseth.testnativa.organization

import com.eelseth.testnativa.base.extensions.applyIoMain
import com.eelseth.testnativa.base.viewModel.BaseViewModel
import com.eelseth.testnativa.model.Organization
import com.eelseth.testnativa.organization.controllers.OrganizationController
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

/**
 * Created by eelSeth on 1/13/2018.
 */

class OrganizationViewModel @Inject constructor(private val organizationController: OrganizationController) : BaseViewModel() {

    private val actionsSubject by lazy { PublishSubject.create<OrganizationUiModel>() }

    override fun subscribeToObservables() {
        compositeDisposable.add(
                organizationController.getAllOrganizations()
                        .applyIoMain()
                        .subscribe({
                            actionsSubject.onNext(OrganizationUiModel.LoadOrganizations(it))
                        }, Throwable::printStackTrace)
        )
    }

    fun actionsSubjectObservable(): Observable<OrganizationUiModel> = actionsSubject

}

sealed class OrganizationUiModel {
    class LoadOrganizations(val organizationList: List<Organization>) : OrganizationUiModel()
}