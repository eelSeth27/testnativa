package com.eelseth.testnativa.organization

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eelseth.testnativa.R
import com.eelseth.testnativa.base.extensions.className
import com.eelseth.testnativa.base.recycler.GenericAdapterRecyclerView
import com.eelseth.testnativa.base.recycler.ViewFactory
import com.eelseth.testnativa.base.ui.fragment.BaseFragment
import com.eelseth.testnativa.databinding.FragmentListBinding
import com.eelseth.testnativa.organization.add.AddOrganizationFragment
import com.eelseth.testnativa.organization.views.OrganizationSummaryView
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by eelSeth on 1/13/2018.
 */

class OrganizationFragment : BaseFragment() {

    private val binding by lazy { DataBindingUtil.inflate<FragmentListBinding>(LayoutInflater.from(context), R.layout.fragment_list, null, false) }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    internal lateinit var viewModel: OrganizationViewModel

    private val organizationsAdapter by lazy {
        GenericAdapterRecyclerView(object : ViewFactory<GenericAdapterRecyclerView.ItemView<*>>() {
            override fun getView(parent: ViewGroup, viewType: Int): GenericAdapterRecyclerView.ItemView<*> =
                    OrganizationSummaryView(parent.context)
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(OrganizationViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        initUi()
        navigationListener?.setToolbarName(R.string.menu_organization)
        lifecycle.addObserver(viewModel)
    }

    private fun initUi() {
        binding.rlList.apply {
            adapter = organizationsAdapter
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        }

        binding.btListAdd.setOnClickListener { openAddPerson() }
    }

    override fun initSubscriptions() {
        super.initSubscriptions()
        compositeDisposable.add(viewModel.actionsSubjectObservable().subscribe(this::handleAction))
    }

    private fun handleAction(action: OrganizationUiModel) {
        when (action) {
            is OrganizationUiModel.LoadOrganizations -> loadData(action)
        }
    }

    private fun openAddPerson() {
        AddOrganizationFragment.newInstance().show(fragmentManager, AddOrganizationFragment.className())
    }

    private fun loadData(loadOrganizations: OrganizationUiModel.LoadOrganizations) {
        if (loadOrganizations.organizationList.isEmpty()) {
            binding.rlList.visibility = View.GONE
            binding.tvEmpty.visibility = View.VISIBLE
        } else {
            binding.rlList.visibility = View.VISIBLE
            binding.tvEmpty.visibility = View.GONE
            organizationsAdapter.items = loadOrganizations.organizationList.map {
                GenericAdapterRecyclerView.ItemModelAbstract(it, 0)
            }
        }


    }
}