package com.eelseth.testnativa.organization.views

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.cardview.widget.CardView
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.TextView
import com.eelseth.testnativa.R
import com.eelseth.testnativa.base.recycler.GenericAdapterRecyclerView
import com.eelseth.testnativa.model.Organization

/**
 * Created by eelSeth on 1/14/2018.
 */
class OrganizationSummaryView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : androidx.cardview.widget.CardView(context, attrs, defStyleAttr), GenericAdapterRecyclerView.ItemView<Organization> {

    lateinit var organization: Organization

    init {
        val marginSize = resources.getDimensionPixelSize(R.dimen.spacing_medium)
        inflate(context, R.layout.item_organization_summary, this)
        layoutParams = MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
            setMargins(marginSize, 0, marginSize, 0)
        }
        useCompatPadding = true
        setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
        radius = resources.getDimensionPixelSize(R.dimen.cardview_default_radius).toFloat()
    }

    override fun bind(item: Organization, position: Int) {
        organization = item

        (findViewById<TextView>(R.id.tv_name)).text = organization.name
        (findViewById<TextView>(R.id.tv_phone)).text = organization.phone
        (findViewById<TextView>(R.id.tv_address)).text = organization.address
    }

    override fun setItemClickListener(onItemClickListener: GenericAdapterRecyclerView.OnItemClickListener?) {}

    override fun getIdForClick(): Int = 0

    override fun getData(): Organization = organization

}