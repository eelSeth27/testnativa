package com.eelseth.testnativa.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

/**
 * Created by eelSeth on 1/13/2018.
 */

@Entity(tableName = "persons")
data class Person(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        var name: String = "",
        var phone: String = "",
        var email: String = "") {

    @Ignore
    constructor() : this(id = 0)
}


