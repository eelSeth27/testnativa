package com.eelseth.testnativa.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by eelSeth on 1/13/2018.
 */

@Entity(tableName = "organizations")
data class Organization(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        var name: String,
        var phone: String,
        var address: String)


