package com.eelseth.testnativa.messaging

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.urbanairship.AirshipReceiver
import com.urbanairship.push.PushMessage


class AppUrbanAirshipReceiver : AirshipReceiver() {

    private val TAG = "events"

    /**
     * Intent action sent as a local broadcast to update the channel.
     */
    val ACTION_UPDATE_CHANNEL = "ACTION_UPDATE_CHANNEL"

    override fun onChannelCreated(context: Context, channelId: String) {
        Log.i(TAG, "Channel created. Channel Id:$channelId.")

        // Broadcast that the channel was created. Used to refresh the channel ID on the home fragment
        LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(ACTION_UPDATE_CHANNEL))
    }

    override fun onChannelUpdated(context: Context, channelId: String) {
        Log.i(TAG, "Channel updated. Channel Id:$channelId.")

        // Broadcast that the channel was updated. Used to refresh the channel ID on the home fragment
        LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(ACTION_UPDATE_CHANNEL))
    }

    override fun onChannelRegistrationFailed(context: Context) {
        Log.i(TAG, "Channel registration failed.")
    }

    override fun onPushReceived(context: Context, message: PushMessage, notificationPosted: Boolean) {
        Log.i(TAG, "Received push message. Alert: " + message.alert + ". posted notification: " + notificationPosted)
    }

    override fun onNotificationPosted(context: Context, notificationInfo: AirshipReceiver.NotificationInfo) {
        Log.i(TAG, "Notification posted. Alert: " + notificationInfo.message.alert + ". NotificationId: " + notificationInfo.notificationId)
    }

    override fun onNotificationOpened(context: Context, notificationInfo: AirshipReceiver.NotificationInfo): Boolean {
        Log.i(TAG, "Notification opened. Alert: " + notificationInfo.message.alert + ". NotificationId: " + notificationInfo.notificationId)

        Log.i("events", "deep>> " + notificationInfo.message)

        // Return false here to allow Urban Airship to auto launch the launcher activity
        return true
    }

    override fun onNotificationOpened(context: Context, notificationInfo: AirshipReceiver.NotificationInfo, actionButtonInfo: AirshipReceiver.ActionButtonInfo): Boolean {
        Log.i(TAG, "Notification action button opened. Button ID: " + actionButtonInfo.buttonId + ". NotificationId: " + notificationInfo.notificationId)

        // Return false here to allow Urban Airship to auto launch the launcher
        // activity for foreground notification action buttons
        return false
    }

    override fun onNotificationDismissed(context: Context, notificationInfo: AirshipReceiver.NotificationInfo) {
        Log.i(TAG, "Notification dismissed. Alert: " + notificationInfo.message.alert + ". Notification ID: " + notificationInfo.notificationId)
    }

    private fun getDeepLinkURIFromMessageBundle(message: PushMessage): Uri? {
        val bundle = message.pushBundle
        val deeplinkStr = bundle.get("deeplink") as String
        return if (TextUtils.isEmpty(deeplinkStr)) null else Uri.parse(deeplinkStr)
    }
}