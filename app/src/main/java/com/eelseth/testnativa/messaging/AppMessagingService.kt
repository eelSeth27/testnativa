package com.eelseth.testnativa.messaging

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.urbanairship.push.fcm.AirshipFirebaseInstanceIdService
import com.urbanairship.push.fcm.AirshipFirebaseMessagingService

class AppMessagingService : FirebaseMessagingService() {

    override fun onNewToken(token: String?) {
        AirshipFirebaseInstanceIdService.processTokenRefresh(this)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        AirshipFirebaseMessagingService.processMessageSync(this, remoteMessage)
    }

}