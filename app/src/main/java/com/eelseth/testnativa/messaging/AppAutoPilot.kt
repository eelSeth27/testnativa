package com.eelseth.testnativa.messaging

import android.content.Context
import android.util.Log
import com.urbanairship.AirshipConfigOptions
import com.urbanairship.Autopilot
import com.urbanairship.UAirship


class AppAutoPilot : Autopilot() {

    override fun onAirshipReady(airship: UAirship) {
        airship.pushManager.userNotificationsEnabled = true
        Log.i("events", "onAirshipReady on channel: " + airship.pushManager.channelId)

        airship.pushManager.tags = setOf("tag.test")
    }

    override fun createAirshipConfigOptions(context: Context): AirshipConfigOptions? {
        return super.createAirshipConfigOptions(context)
    }

}