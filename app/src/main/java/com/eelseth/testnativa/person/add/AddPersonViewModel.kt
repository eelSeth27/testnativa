package com.eelseth.testnativa.person.add

import androidx.lifecycle.ViewModel
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.util.Log
import com.eelseth.testnativa.base.NO_ERROR
import com.eelseth.testnativa.base.extensions.applyIoMain
import com.eelseth.testnativa.base.extensions.className
import com.eelseth.testnativa.model.Person
import com.eelseth.testnativa.person.controllers.PersonController
import com.eelseth.testnativa.util.*
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.*
import javax.inject.Inject

/**
 * Created by eelSeth on 1/13/2018.
 */

class AddPersonViewModel @Inject constructor(private val personController: PersonController) : ViewModel() {

    private val actionsSubject by lazy { PublishSubject.create<AddPersonUiModel>() }

    val name = ObservableField("")
    val phone = ObservableField("")
    val email = ObservableField("")

    val errorName = ObservableInt(NO_ERROR)
    val errorPhone = ObservableInt(NO_ERROR)
    val errorEmail = ObservableInt(NO_ERROR)

    val loadingVisibility = ObservableBoolean(false)

    fun validateForm() {
        if (isFormValid(errorName.isFieldFilled(name.getData()),
                        errorPhone.isFieldFilled(phone.getData()),
                        errorEmail.isFieldEmail(email.getData()))) {
            save()
        }
    }

    private fun save() {
        personController.savePerson(Person(0, name.getData(), phone.getData(), email.getData()))
                .doOnSubscribe { loadingVisibility.set(true) }
                .delay(1L, TimeUnit.SECONDS)
                .applyIoMain()
                .subscribe({
                    Log.e(this.className(), "Inserted 1 item in DB...")
                    loadingVisibility.set(false)
                    actionsSubject.onNext(AddPersonUiModel.OnSuccessfullySaved)
                    clearForm()
                }, {
                    it.printStackTrace()
                    loadingVisibility.set(false)
                })
    }

    private fun clearForm() {
        clearFormFields(name, phone, email)
    }

    fun actionsSubjectObservable(): Observable<AddPersonUiModel> = actionsSubject
}

sealed class AddPersonUiModel {
    object OnSuccessfullySaved : AddPersonUiModel()
}