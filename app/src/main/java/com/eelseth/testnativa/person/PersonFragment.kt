package com.eelseth.testnativa.person

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eelseth.testnativa.R
import com.eelseth.testnativa.base.extensions.className
import com.eelseth.testnativa.base.recycler.GenericAdapterRecyclerView
import com.eelseth.testnativa.base.recycler.ViewFactory
import com.eelseth.testnativa.base.ui.fragment.BaseFragment
import com.eelseth.testnativa.databinding.FragmentListBinding
import com.eelseth.testnativa.model.Person
import com.eelseth.testnativa.person.add.AddPersonFragment
import com.eelseth.testnativa.person.views.PersonSummaryView
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


/**
 * Created by eelSeth on 1/13/2018.
 */

class PersonFragment : BaseFragment() {

    private val binding by lazy { DataBindingUtil.inflate<FragmentListBinding>(LayoutInflater.from(context), R.layout.fragment_list, null, false) }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PersonViewModel

    private val personsAdapter by lazy {
        GenericAdapterRecyclerView(object : ViewFactory<GenericAdapterRecyclerView.ItemView<*>>() {
            override fun getView(parent: ViewGroup, viewType: Int): GenericAdapterRecyclerView.ItemView<*> =
                PersonSummaryView(parent.context).also { Log.i("events", "printed") }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PersonViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        initUi()
        navigationListener?.setToolbarName(R.string.menu_person)
        lifecycle.addObserver(viewModel)
    }

    private fun initUi() {
        binding.rlList.apply {
            adapter = personsAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }

        binding.btListAdd.setOnClickListener {
            //openAddPerson()

            val list = generateSequence(0) { it + 1 }
                .take(50)
                .map { Person(1, "a$it", "----") }
                .toList()


            loadData(PersonUiModel.LoadPersons(personList = list))
        }
    }

    override fun initSubscriptions() {
        super.initSubscriptions()
        compositeDisposable.add(viewModel.actionsSubjectObservable().subscribe(this::handleAction))
    }

    private fun handleAction(action: PersonUiModel) {
        when (action) {
            is PersonUiModel.LoadPersons -> loadData(action)
        }
    }

    private fun openAddPerson() {
        AddPersonFragment.newInstance().show(fragmentManager, AddPersonFragment.className())
    }

    private fun loadData(loadPersons: PersonUiModel.LoadPersons) {
        if (loadPersons.personList.isEmpty()) {
            binding.rlList.visibility = View.GONE
            binding.tvEmpty.visibility = View.VISIBLE
        } else {
            binding.rlList.visibility = View.VISIBLE
            binding.tvEmpty.visibility = View.GONE
            personsAdapter.items = loadPersons.personList.map {
                GenericAdapterRecyclerView.ItemModelAbstract(it, 0)
            }
        }


    }
}