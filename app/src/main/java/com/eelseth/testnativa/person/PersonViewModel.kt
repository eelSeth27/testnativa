package com.eelseth.testnativa.person

import com.eelseth.testnativa.base.extensions.applyIoMain
import com.eelseth.testnativa.base.viewModel.BaseViewModel
import com.eelseth.testnativa.model.Person
import com.eelseth.testnativa.person.controllers.PersonController
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

/**
 * Created by eelSeth on 1/13/2018.
 */

class PersonViewModel @Inject constructor(private val personController: PersonController) : BaseViewModel() {

    private val actionsSubject by lazy { PublishSubject.create<PersonUiModel>() }

    override fun subscribeToObservables() {
        compositeDisposable.add(
                personController.getAllPersons()
                        .applyIoMain()
                        .subscribe({
                            actionsSubject.onNext(PersonUiModel.LoadPersons(it))
                        }, Throwable::printStackTrace)
        )
    }

    fun actionsSubjectObservable(): Observable<PersonUiModel> = actionsSubject

}

sealed class PersonUiModel {
    class LoadPersons(val personList: List<Person>) : PersonUiModel()
}