package com.eelseth.testnativa.person.views

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.cardview.widget.CardView
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.TextView
import com.eelseth.testnativa.R
import com.eelseth.testnativa.base.recycler.GenericAdapterRecyclerView
import com.eelseth.testnativa.model.Person

/**
 * Created by eelSeth on 1/14/2018.
 */
class PersonSummaryView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : androidx.cardview.widget.CardView(context, attrs, defStyleAttr), GenericAdapterRecyclerView.ItemView<Person> {

    lateinit var person: Person

    init {
        val marginSize = resources.getDimensionPixelSize(R.dimen.spacing_medium)
        inflate(context, R.layout.item_person_summary, this)
        layoutParams = MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
            setMargins(marginSize, 0, marginSize, 0)
        }
        useCompatPadding = true
        setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
        radius = resources.getDimensionPixelSize(R.dimen.cardview_default_radius).toFloat()
    }

    override fun bind(item: Person, position: Int) {
        person = item

        (findViewById<TextView>(R.id.tv_name)).text = person.name
        (findViewById<TextView>(R.id.tv_phone)).text = person.phone
        (findViewById<TextView>(R.id.tv_email)).text = person.email
    }

    override fun setItemClickListener(onItemClickListener: GenericAdapterRecyclerView.OnItemClickListener?) {}

    override fun getIdForClick(): Int = 0

    override fun getData(): Person = person

}