package com.eelseth.testnativa.person.controllers

import com.eelseth.testnativa.model.Person
import com.eelseth.testnativa.persistence.AppDatabase
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by eelSeth on 2/27/2018.
 */

@Singleton
class PersonController @Inject constructor(private val appDatabase: AppDatabase,
                                           private val cloudDatabase: DatabaseReference) {

    fun getAllPersons(): Flowable<List<Person>> = appDatabase.personDao().getAllPersons()

    fun getCloudAllPersons(): Flowable<List<Person>> {
        return Flowable.create(
                { emitter ->
                    cloudDatabase.addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(error: DatabaseError) {
                        }

                        override fun onDataChange(data: DataSnapshot) {
                            val personList = ArrayList<Person>()
                            data.children.forEach {
                                it.getValue(Person::class.java)?.let { personList.add(it) }
                            }
                            emitter.onNext(personList)
                        }
                    })
                }, BackpressureStrategy.BUFFER)
    }

    fun savePerson(person: Person): Observable<Unit> =
            Observable.fromCallable { appDatabase.personDao().insertPerson(person) }

}