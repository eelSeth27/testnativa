package com.eelseth.testnativa.base.ui.fragment

import androidx.annotation.StringRes
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by eelSeth on 1/14/2018.
 */
open class BaseDialogFragment : androidx.fragment.app.DialogFragment() {

    val compositeDisposable by lazy { CompositeDisposable() }
    private val parentActivity by lazy { activity as AppCompatActivity }

    open fun initSubscriptions() {}

    override fun onStart() {
        super.onStart()
        initSubscriptions()
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    fun setUpToolbar(toolbar: Toolbar, @StringRes title: Int) {
        parentActivity.setSupportActionBar(toolbar)
        parentActivity.supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
        toolbar.apply {
            setTitle(title)
            setNavigationOnClickListener { dismiss() }
        }
    }
}