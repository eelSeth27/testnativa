package com.eelseth.testnativa.base.extensions

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by eelSeth on 2/27/2018.
 */

fun <T> Flowable<T>.applyIoMain(): Flowable<T> {
    return subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.applyIoMain(): Observable<T> {
    return subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Flowable<T>.applyComputationNewThread(): Flowable<T> {
    return subscribeOn(Schedulers.computation())
            .observeOn(Schedulers.newThread())
}

fun <T> Observable<T>.applyComputationNewThread(): Observable<T> {
    return subscribeOn(Schedulers.computation())
            .observeOn(Schedulers.newThread())
}