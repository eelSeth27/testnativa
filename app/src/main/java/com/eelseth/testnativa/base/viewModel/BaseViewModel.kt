package com.eelseth.testnativa.base.viewModel

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import android.util.Log
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by eelSeth on 1/14/2018.
 */
open class BaseViewModel : ViewModel(), LifecycleObserver {

    val compositeDisposable by lazy { CompositeDisposable() }

    protected open fun subscribeToObservables() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun onStart() {
        Log.i("life", "onStart")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun onStop() {
        Log.i("life", "onStop")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onResume() {
        subscribeToObservables()
        Log.i("life", "onResume")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onPause() {
        Log.i("life", "onPause")
        compositeDisposable.clear()
    }

    override fun onCleared() {
        compositeDisposable.dispose()
    }

}