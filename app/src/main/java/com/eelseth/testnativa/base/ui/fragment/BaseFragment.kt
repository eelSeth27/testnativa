package com.eelseth.testnativa.base.ui.fragment

import androidx.fragment.app.Fragment
import com.eelseth.testnativa.base.ui.listeners.INavigationListener
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by eelSeth on 1/14/2018.
 */
open class BaseFragment : androidx.fragment.app.Fragment() {

    val navigationListener by lazy { activity as INavigationListener? }
    val compositeDisposable by lazy { CompositeDisposable() }

    open fun initSubscriptions() {}

    override fun onResume() {
        super.onResume()
        initSubscriptions()
    }

    override fun onPause() {
        super.onPause()
        compositeDisposable.clear()
    }
}