package com.eelseth.testnativa.base.recycler;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class GenericAdapterRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ViewFactory viewFactory;
    private List<ItemModel> items;
    private OnItemClickListener onItemClickListener;

    public GenericAdapterRecyclerView(ViewFactory viewFactory) {
        this.viewFactory = viewFactory;
        items = new ArrayList<>();
    }

    public void setItems(List<? extends ItemModel> items) {
        this.items = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (items != null) {
            items.clear();
            items = new ArrayList<>();
            notifyDataSetChanged();
        }
    }

    public ItemModel getItemAt(int position) {
        return items.get(position);
    }

    public void addItem(int index, ItemModel itemToAdd) {
        items.add(index, itemToAdd);
        notifyItemInserted(index);
    }

    public void addItem(ItemModel itemToAdd) {
        items.add(itemToAdd);
        notifyItemInserted(items.size() - 1);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public List<? extends ItemModel> getItems() {
        return this.items;
    }

    @Override
    public int getItemViewType(int position) {
        return (items != null && items.size() > position) ? items.get(position).getViewType() : -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(viewFactory.getView(parent, viewType));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemView itemView = (ItemView) holder.itemView;
        if (items.get(position) != null) {
            itemView.bind(items.get(position).getData(), position);
        }
        itemView.setItemClickListener(onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public interface ItemView<T> {
        void bind(T item, int position);

        void setItemClickListener(OnItemClickListener onItemClickListener);

        int getIdForClick();

        T getData();
    }

    public interface ItemModel<T> {
        T getData();

        void setData(T data);

        int getViewType();
    }

    public interface OnItemClickListener {
        void onItemClicked(ItemView itemView);

        void onItemLongClicked(ItemView itemView);
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private ViewHolder(ItemView itemView) {
            super((View) itemView);
        }
    }

    public static class ItemModelAbstract implements ItemModel<Object> {

        private Object data;
        private int viewType;

        public ItemModelAbstract(Object data, int viewType) {
            this.data = data;
            this.viewType = viewType;
        }

        public ItemModelAbstract(int viewType) {
            this.viewType = viewType;
        }

        @Override
        public void setData(Object data) {
            this.data = data;
        }

        @Override
        public Object getData() {
            return data;
        }

        @Override
        public int getViewType() {
            return viewType;
        }

    }

}
