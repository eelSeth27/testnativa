package com.eelseth.testnativa.base.ui.listeners

import androidx.annotation.StringRes

/**
 * Created by eelSeth on 1/14/2018.
 */
interface INavigationListener {

    fun setToolbarName(@StringRes titleId: Int)
}