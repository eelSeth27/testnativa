package com.eelseth.testnativa.base.extensions

import android.util.Patterns

/**
 * Created by eelSeth on 1/13/2018.
 */

fun Any.className(): String = this::class.java.name

fun String.isValidEmail(): Boolean = this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

