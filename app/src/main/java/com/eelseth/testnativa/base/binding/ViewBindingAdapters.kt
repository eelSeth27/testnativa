package com.eelseth.testnativa.base.binding

import androidx.databinding.BindingAdapter
import android.graphics.PorterDuff
import androidx.annotation.ColorRes
import com.google.android.material.textfield.TextInputLayout
import androidx.core.content.ContextCompat
import android.widget.ImageView
import com.eelseth.testnativa.base.NO_ERROR

/**
 * Created by eelSeth on 1/13/2018.
 */

object ViewBindingAdapters {

    @JvmStatic
    @BindingAdapter(value = ["img_color_filter"])
    fun setColorFilter(imageView: ImageView, @ColorRes color: Int) {
        imageView.setColorFilter(ContextCompat.getColor(imageView.context, color), PorterDuff.Mode.SRC_IN)
    }

    @JvmStatic
    @BindingAdapter("app:errorText")
    fun setErrorMessage(view: TextInputLayout, errorMessageResource: Int) =
            if (errorMessageResource != NO_ERROR) {
                view.error = view.context.getString(errorMessageResource)
            } else {
                view.error = ""
            }
}

