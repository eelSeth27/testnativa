package com.eelseth.testnativa.util

/**
 * Created by eelSeth on 1/13/2018.
 */

const val HOME_MENU_OPTION = 0x01
const val PERSON_MENU_OPTION = 0x02
const val ORGANIZATION_MENU_OPTION = 0x03
const val BUSINESS_MENU_OPTION = 0x04
const val TASK_MENU_OPTION = 0x05