package com.eelseth.testnativa.util

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.annotation.StringRes
import com.eelseth.testnativa.R
import com.eelseth.testnativa.base.NO_ERROR
import com.eelseth.testnativa.base.extensions.isValidEmail

/**
 * Created by eelSeth on 2/27/2018.
 */


fun isValidField(isValid: Boolean, field: ObservableInt, @StringRes errorResource: Int = NO_ERROR): Boolean {
    return isValid.also {
        field.set(if (isValid) NO_ERROR else errorResource)
    }
}

fun isFormValid(vararg validations: Boolean): Boolean = validations.all { it }

fun ObservableInt.isFieldFilled(data: String): Boolean = isValidField(data.isNotBlank(), this, R.string.error_required_field)

fun ObservableInt.isFieldEmail(data: String): Boolean = isValidField(data.isValidEmail(), this, R.string.error_invalid_email)

fun clearFormFields(vararg fields: ObservableField<String>) {
    fields.forEach { it.set("") }
}

fun ObservableField<String>.getData(): String = this.get() ?: ""