package com.eelseth.testnativa.home

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.eelseth.testnativa.R
import com.eelseth.testnativa.base.ui.fragment.BaseFragment

/**
 * Created by eelSeth on 1/13/2018.
 */

class HomeFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        layoutInflater.inflate(R.layout.content_home, null)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigationListener?.setToolbarName(R.string.menu_home)

        AlertDialog.Builder(requireContext(), R.style.AppAlertDialogTheme).setMessage("asdasd")
            .setNegativeButton("ok", DialogInterface.OnClickListener { dialog, which -> })
            .setTitle("xxxx")
            .create()
            .show()
    }

}