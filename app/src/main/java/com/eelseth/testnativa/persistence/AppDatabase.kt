package com.eelseth.testnativa.persistence

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import com.eelseth.testnativa.model.Organization
import com.eelseth.testnativa.model.Person

/**
 * Created by eelSeth on 1/13/2018.
 */

@Database(entities = [(Person::class), (Organization::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun personDao(): PersonDao
    abstract fun organizationDao(): OrganizationDao

    companion object {
        private val APP_DB = "app.db"

        @Volatile private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, APP_DB)
                        .build()
    }
}