package com.eelseth.testnativa.persistence

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.eelseth.testnativa.model.Organization
import io.reactivex.Flowable

/**
 * Created by eelSeth on 1/13/2018.
 */

@Dao
interface OrganizationDao {

    @Query("SELECT * FROM organizations")
    fun getAllOrganizations(): Flowable<List<Organization>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrganization(person: Organization)
}