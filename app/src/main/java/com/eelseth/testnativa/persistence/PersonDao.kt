package com.eelseth.testnativa.persistence

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.eelseth.testnativa.model.Person
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by eelSeth on 1/13/2018.
 */

@Dao
interface PersonDao {

    @Query("SELECT * FROM persons")
    fun getAllPersons(): Flowable<List<Person>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPerson(person: Person)
}