package com.eelseth.testnativa

import android.app.Activity
import android.app.Application
import android.content.Context
import com.eelseth.testnativa.di.AppComponent
import com.eelseth.testnativa.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by eelSeth on 1/13/2018.
 */

class ApplicationHandler : Application(), HasActivityInjector {

    init {
        instance = this
    }

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    lateinit var appComponent: AppComponent

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()
        initializeInjector()
    }

    private fun initializeInjector() {
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)
    }

    companion object {
        private lateinit var instance: ApplicationHandler

        fun applicationContext(): Context {
            return instance.applicationContext
        }
    }

}