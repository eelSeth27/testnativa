package com.eelseth.testnativa

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import com.eelseth.testnativa.base.ui.listeners.INavigationListener
import com.eelseth.testnativa.databinding.ActivityHomeBinding
import com.eelseth.testnativa.home.HomeFragment
import com.eelseth.testnativa.organization.OrganizationFragment
import com.eelseth.testnativa.person.PersonFragment
import com.eelseth.testnativa.util.HOME_MENU_OPTION
import com.eelseth.testnativa.util.ORGANIZATION_MENU_OPTION
import com.eelseth.testnativa.util.PERSON_MENU_OPTION
import com.google.android.material.navigation.NavigationView
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

const val CURRENT_OPTION_ARG = "current_option"

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, INavigationListener, HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<androidx.fragment.app.Fragment>

    private val binding: ActivityHomeBinding by lazy { DataBindingUtil.setContentView<ActivityHomeBinding>(this, R.layout.activity_home) }
    private var currentOptionMenu: Int = HOME_MENU_OPTION

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setUpToolbar()
        setUpDrawer()
        savedInstanceState?.let { currentOptionMenu = it.getInt(CURRENT_OPTION_ARG) }
            ?: loadMenuOption(currentOptionMenu)
    }

    private fun setUpToolbar() {
        setSupportActionBar(binding.toolbar)
    }

    private fun setUpDrawer() {
        ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close).also {
            binding.drawerLayout.addDrawerListener(it)
            it.syncState()
        }
        binding.navMenu.setNavigationItemSelectedListener(this)
    }

    private fun loadMenuOption(menuOption: Int) {
        currentOptionMenu = menuOption
        when (menuOption) {
            HOME_MENU_OPTION -> {
                loadFragment(HomeFragment())
            }
            PERSON_MENU_OPTION -> loadFragment(PersonFragment())
            ORGANIZATION_MENU_OPTION -> loadFragment(OrganizationFragment())
        }
    }

    private fun loadFragment(fragment: androidx.fragment.app.Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_content, fragment)
            .commit()
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun setToolbarName(titleId: Int) {
        binding.toolbar.setTitle(titleId)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> loadMenuOption(HOME_MENU_OPTION)
            R.id.nav_person -> loadMenuOption(PERSON_MENU_OPTION)
            R.id.nav_organization -> loadMenuOption(ORGANIZATION_MENU_OPTION)
            R.id.nav_business, R.id.nav_task -> showAlertMessage()
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun showAlertMessage() {
        Toast.makeText(this, "To be implemented", Toast.LENGTH_LONG).show()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt(CURRENT_OPTION_ARG, currentOptionMenu)
        super.onSaveInstanceState(outState)
    }

    override fun supportFragmentInjector(): AndroidInjector<androidx.fragment.app.Fragment> = fragmentDispatchingAndroidInjector
}
