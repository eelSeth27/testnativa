package com.eelseth.testnativa

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity


class DeepLinkActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deeplink)

        Log.i("events", "DeepLinkActivity started")

        val propertyId = intent.data?.lastPathSegment
        Log.i("events", "propertyId: $propertyId")
    }




}
