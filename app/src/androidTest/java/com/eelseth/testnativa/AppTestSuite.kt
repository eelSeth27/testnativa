package com.eelseth.testnativa

import com.eelseth.testnativa.person.PersonDaoTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

/**
 * Created by eelSeth on 2/4/2018.
 */

@RunWith(Suite::class)
@Suite.SuiteClasses(
        PersonDaoTest::class
)
class AppTestSuite