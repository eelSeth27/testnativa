package com.eelseth.testnativa.person

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.eelseth.testnativa.model.Person
import com.eelseth.testnativa.persistence.AppDatabase
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by eelSeth on 2/4/2018.
 */

@RunWith(AndroidJUnit4::class)
class PersonDaoTest {

    @JvmField
    @Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var appDatabase: AppDatabase

    @Before
    fun initDatabase() {
        appDatabase = Room.inMemoryDatabaseBuilder(
                InstrumentationRegistry.getContext(),
                AppDatabase::class.java)
                .allowMainThreadQueries()
                .build()
    }

    @After
    fun closeDatabase() {
        appDatabase.close()
    }

    @Test
    fun savePersonData() {
        appDatabase.personDao().insertPerson(PERSON_INSERT)

        appDatabase.personDao()
                .getAllPersons()
                .test()
                .assertValue {
                    it[0] == PERSON_INSERT
                }
    }


    companion object {

        private val PERSON_INSERT = Person(
                1,
                "Alvaro",
                "3012174152",
                "alv1gz@gmail.com")
    }

}