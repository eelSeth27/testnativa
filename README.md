# TestNativa - Android

TestNativa library that simplifies the access to services offered by Test such as insurance, investment management, and other financial services.  


## Installation

You can download the axa.aar file from [link]()

Import the library in your project

````
#!kotlin
Test.init(context, "baseUrl", "tokenProvided")
````


## Usage

### Initial config

In order to start accessing the services, first you need to initialize the library with the information regards connection

````
#!groovy
implementation(name:'axa', ext:'aar')
````

Using the Fast Android Networking with Jackson Parser
```groovy
compile 'com.amitshekhar.android:jackson-android-networking:1.0.1'
implementation(name:'axa', ext:'aar')
```

### Adding common data

